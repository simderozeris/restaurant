package asw.efood.restaurantservice.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import asw.efood.common.api.event.DomainEvent;
import asw.efood.common.api.event.RestaurantServiceEventChannel;

@Component
public class RestaurantDomainEventPublisherImpl implements RestaurantDomainEventPublisher {
	@Autowired
	private KafkaTemplate<String, DomainEvent> template;
	private String channel = RestaurantServiceEventChannel.channel;
	public void publish(DomainEvent event) {
		template.send(channel, event);
	}
}
