package asw.efood.restaurantservice.domain;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import asw.efood.common.api.event.RestaurantCreatedEvent;
import asw.efood.common.api.event.RestaurantServiceEventChannel;

@Component
public class RestaurantDomainEventConsumer {

	@KafkaListener(topics = RestaurantServiceEventChannel.channel)
	public void listen(ConsumerRecord<String, RestaurantCreatedEvent> record) throws Exception {
		RestaurantCreatedEvent event = record.value();
		System.out.println("E' stato creato un nuovo ristorante: " + event.toString());
	}
}
