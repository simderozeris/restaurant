package asw.efood.restaurantservice.domain;

import org.springframework.data.repository.CrudRepository;

public interface MenuRepository extends CrudRepository<RestaurantMenu, Long> {
}
