package asw.efood.restaurantservice.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MenuItemService {

	@Autowired
	private MenuItemRepository menuItemRepository;

 	public MenuItem createMenuItem(String name,double price,RestaurantMenu menu) {
 		MenuItem menuItem = new MenuItem(name,price,menu);
 		menuItem = menuItemRepository.save(menuItem);
		return menuItem;
	}

 	public MenuItem getMenuItem(Long id) {
 		MenuItem menuItem = menuItemRepository.findById(id).orElse(null);
		return menuItem;
	}
 	
 	public MenuItem updateMenuItem(MenuItem menuItem) {
 		MenuItem menuItemUpdate = menuItemRepository.save(menuItem);
		return menuItemUpdate;
	}
}

	
