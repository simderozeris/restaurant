package asw.efood.restaurantservice.domain;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Data; 


@Entity 
@Data
public class RestaurantMenu {
	@Id 
	@GeneratedValue
	private Long id; 
	
    @OneToMany(mappedBy = "menu", fetch = FetchType.EAGER)
	private List<MenuItem> listMenuItem;
    
    @OneToOne
    private Restaurant restaurant;
	
	public RestaurantMenu(Restaurant restaurant, List<MenuItem> listMenuItem) {
		this(); 
		this.restaurant = restaurant;
		this.listMenuItem = listMenuItem;
	}
	
	public RestaurantMenu() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<MenuItem> getListMenuItem() {
		return listMenuItem;
	}
	public void setListMenuItem(List<MenuItem> listMenuItem) {
		this.listMenuItem = listMenuItem;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}	
}
