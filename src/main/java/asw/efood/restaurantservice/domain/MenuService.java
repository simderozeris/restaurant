package asw.efood.restaurantservice.domain;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MenuService {

	@Autowired
	private MenuRepository menuRepository;

 	public RestaurantMenu createMenu(Restaurant restaurant,List<MenuItem> listMenuItem) {
 		RestaurantMenu menu = new RestaurantMenu(restaurant,listMenuItem);
 		menu = menuRepository.save(menu);
		return menu;
	}

 	public RestaurantMenu getMenu(Long id) {
		RestaurantMenu menu = menuRepository.findById(id).orElse(null);
		return menu;
	}

}

	
