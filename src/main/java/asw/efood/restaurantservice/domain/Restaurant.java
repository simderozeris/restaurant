package asw.efood.restaurantservice.domain;

import javax.persistence.*; 

import lombok.*; 

@Entity 
@Data
public class Restaurant {

	@Id 
	@GeneratedValue
	private Long id; 
	private String name; 
	private String location; 
	
    @OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="menu_id")
	private RestaurantMenu menu;
	
	public Restaurant(String name, String location) {
		this(); 
		this.name = name; 
		this.location = location; 
	}

	public Restaurant() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public RestaurantMenu getMenu() {
		return menu;
	}

	public void setMenu(RestaurantMenu menu) {
		this.menu = menu;
	}
}
