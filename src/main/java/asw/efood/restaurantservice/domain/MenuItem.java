package asw.efood.restaurantservice.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity 
@Data
public class MenuItem {
	@Id 
	@GeneratedValue
	private Long id; 
	private String name; 
	private double price; 
	
	@ManyToOne
	@JoinColumn(name = "menu_id", nullable = false)
	private RestaurantMenu menu;

	public MenuItem(String name, double price,RestaurantMenu menu) {
		this(); 
		this.name = name; 
		this.price = price; 
		this.menu = menu;
	}

	public MenuItem() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public RestaurantMenu getMenu() {
		return menu;
	}

	public void setMenu(RestaurantMenu menu) {
		this.menu = menu;
	}
}