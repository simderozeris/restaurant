package asw.efood.restaurantservice.api.rest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import asw.efood.restaurantservice.domain.MenuItem;
import asw.efood.restaurantservice.domain.MenuItemService;
import asw.efood.restaurantservice.domain.MenuService;
import asw.efood.restaurantservice.domain.Restaurant;
import asw.efood.restaurantservice.domain.RestaurantMenu;
import asw.efood.restaurantservice.domain.RestaurantService;

@RestController
@RequestMapping(path="/rest")
public class RestaurantRestController {
	
	@Autowired
	private RestaurantService restaurantService;
	
	@Autowired
	private MenuService menuService;
	
	@Autowired
	private MenuItemService menuItemService;
	
	@GetMapping("/restaurants/{restaurantId}")
	public GetRestaurantResponse getRestaurant(@PathVariable Long restaurantId) {
		Restaurant restaurant = restaurantService.getRestaurant(restaurantId);
		if (restaurant!=null) {
			GetRestaurantResponse response = restaurantToGetRestaurantResponse(restaurant);
			return response;
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Restaurant not found");
		}
	}
	
	@GetMapping("/restaurants")
	public GetRestaurantsResponse getRestaurants() {
		Collection<Restaurant> restaurants =restaurantService.getAllRestaurants();
		Collection<GetRestaurantResponse> restaurantResponses = restaurants.stream().map(r -> restaurantToGetRestaurantResponse(r)).collect(Collectors.toList());
		return new GetRestaurantsResponse(restaurantResponses);
	}
	
	@PostMapping("/restaurants")
	public CreateRestaurantResponse createRestaurant(@RequestBody CreateRestaurantRequest request) {
		String name = request.getName();
		String location = request.getLocation();
		Restaurant restaurant =restaurantService.createRestaurant(name, location);
		CreateRestaurantResponse response = new CreateRestaurantResponse(restaurant.getId());
		return response;
	}
	
	private GetRestaurantResponse restaurantToGetRestaurantResponse(Restaurant restaurant) {
		return new GetRestaurantResponse(restaurant.getId(),restaurant.getName(),restaurant.getLocation());
	}
	
	private CreateRestaurantResponse restaurantToCreateRestaurantResponse(Restaurant restaurant) {
		return new CreateRestaurantResponse(restaurant.getId());
	}
	
	@GetMapping("/menu/{menuId}")
	public GetRestaurantMenuResponse getMenu(@PathVariable Long menuId) {
		RestaurantMenu menu = menuService.getMenu(menuId);
		if (menu!=null) {
			List<GetMenuItemResponse> menuItemResponses = menu.getListMenuItem().stream().map(m -> menuItemToGetMenuItemResponse(m)).collect(Collectors.toList());
			return new GetRestaurantMenuResponse(menu.getId(),menuItemResponses, restaurantToGetRestaurantResponse(menu.getRestaurant()));
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Restaurant not found");
		}
	}
	
	private GetMenuItemResponse menuItemToGetMenuItemResponse(MenuItem menuItem) {
		return new GetMenuItemResponse(menuItem.getId(), menuItem.getName(), menuItem.getPrice());
	}
	
	@PostMapping("/menu/{restaurantId}")
	public CreateMenuResponse createMenu(@PathVariable Long restaurantId) {
		Restaurant restaurant = restaurantService.getRestaurant(restaurantId);
		RestaurantMenu menu = menuService.createMenu(restaurant,new ArrayList<MenuItem>());
		restaurant.setMenu(menu);
		restaurant = restaurantService.update(restaurant);	
		return menuToCreateMenuResponse(menu);	
	}
	
	@PostMapping("/menu/menuitem/{menuId}")
	public CreateMenuItemResponse createMenuItem(@RequestBody CreateMenuItemRequest request,@PathVariable Long menuId) {
		RestaurantMenu menu = menuService.getMenu(menuId);	
		MenuItem menuItem = menuItemService.createMenuItem(request.getName(), request.getPrice(),menu);
		return new CreateMenuItemResponse(menuItem.getId(), menuItem.getName(), menuItem.getPrice());
	}
	
	private CreateMenuItemResponse menuItemToCreateMenuItemResponse(MenuItem menuItem) {
		return new CreateMenuItemResponse(menuItem.getId(), menuItem.getName(), menuItem.getPrice());
	}
	
	private CreateMenuResponse menuToCreateMenuResponse(RestaurantMenu menu) {
		List<CreateMenuItemResponse> menuItemResponses = menu.getListMenuItem().stream().map(m -> menuItemToCreateMenuItemResponse(m)).collect(Collectors.toList());
		return new CreateMenuResponse(menu.getId(), menuItemResponses, restaurantToCreateRestaurantResponse(menu.getRestaurant()));
	}
}
