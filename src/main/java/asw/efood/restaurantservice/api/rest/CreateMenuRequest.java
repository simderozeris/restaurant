package asw.efood.restaurantservice.api.rest;

import java.util.List;

public class CreateMenuRequest {
	private Long id; 
	private List<CreateMenuItemRequest> listMenuItem;
	
	public CreateMenuRequest(Long id, List<CreateMenuItemRequest> listMenuItem) {
		super();
		this.id = id;
		this.listMenuItem = listMenuItem;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<CreateMenuItemRequest> getListMenuItem() {
		return listMenuItem;
	}
	public void setListMenuItem(List<CreateMenuItemRequest> listMenuItem) {
		this.listMenuItem = listMenuItem;
	}
}
