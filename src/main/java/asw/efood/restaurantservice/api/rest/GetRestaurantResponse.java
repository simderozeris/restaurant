package asw.efood.restaurantservice.api.rest;

public class GetRestaurantResponse {
	private Long restaurantId;
	private String name;
	private String location;

	public GetRestaurantResponse(Long restaurantId, String name, String location) {
		super();
		this.restaurantId = restaurantId;
		this.name = name;
		this.location = location;
	}
	public Long getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(Long restaurantId) {
		this.restaurantId = restaurantId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
}
