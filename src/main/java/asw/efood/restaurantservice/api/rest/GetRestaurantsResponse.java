package asw.efood.restaurantservice.api.rest;

import java.util.Collection;

public class GetRestaurantsResponse {
	private Collection<GetRestaurantResponse> restaurants;

	public GetRestaurantsResponse(Collection<GetRestaurantResponse> restaurants) {
		super();
		this.restaurants = restaurants;
	}

	public Collection<GetRestaurantResponse> getRestaurants() {
		return restaurants;
	}

	public void setRestaurants(Collection<GetRestaurantResponse> restaurants) {
		this.restaurants = restaurants;
	}
}
