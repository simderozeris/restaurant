package asw.efood.restaurantservice.api.rest;

import java.util.List;

public class GetRestaurantMenuResponse {
	
	private Long id; 
	private List<GetMenuItemResponse> listMenuItem;
    private GetRestaurantResponse restaurantResponse;
    
	public GetRestaurantMenuResponse(Long id, List<GetMenuItemResponse> listMenuItem, GetRestaurantResponse restaurantResponse) {
		super();
		this.id = id;
		this.listMenuItem = listMenuItem;
		this.restaurantResponse = restaurantResponse;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<GetMenuItemResponse> getListMenuItem() {
		return listMenuItem;
	}

	public void setListMenuItem(List<GetMenuItemResponse> listMenuItem) {
		this.listMenuItem = listMenuItem;
	}

	public GetRestaurantResponse getRestaurantResponse() {
		return restaurantResponse;
	}

	public void setRestaurantResponse(GetRestaurantResponse restaurantResponse) {
		this.restaurantResponse = restaurantResponse;
	}
}
