package asw.efood.restaurantservice.api.rest;

public class CreateRestaurantResponse {
	private Long restaurantId;

	public CreateRestaurantResponse(Long restaurantId) {
		super();
		this.restaurantId = restaurantId;
	}

	public Long getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Long restaurantId) {
		this.restaurantId = restaurantId;
	}
}
