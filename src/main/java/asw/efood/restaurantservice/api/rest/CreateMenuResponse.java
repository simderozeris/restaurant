package asw.efood.restaurantservice.api.rest;

import java.util.List;

public class CreateMenuResponse {
	private Long id; 
	private List<CreateMenuItemResponse> listMenuItem;
	private CreateRestaurantResponse restaurant;
	
	public CreateMenuResponse(Long id, List<CreateMenuItemResponse> listMenuItem, CreateRestaurantResponse restaurant) {
		super();
		this.id = id;
		this.listMenuItem = listMenuItem;
		this.restaurant = restaurant;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public List<CreateMenuItemResponse> getListMenuItem() {
		return listMenuItem;
	}
	public void setListMenuItem(List<CreateMenuItemResponse> listMenuItem) {
		this.listMenuItem = listMenuItem;
	}
	public CreateRestaurantResponse getRestaurant() {
		return restaurant;
	}
	public void setRestaurant(CreateRestaurantResponse restaurant) {
		this.restaurant = restaurant;
	}
}
