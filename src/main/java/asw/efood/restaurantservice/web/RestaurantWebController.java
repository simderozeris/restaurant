package asw.efood.restaurantservice.web;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import asw.efood.restaurantservice.domain.MenuItem;
import asw.efood.restaurantservice.domain.MenuItemService;
import asw.efood.restaurantservice.domain.MenuService;
import asw.efood.restaurantservice.domain.Restaurant;
import asw.efood.restaurantservice.domain.RestaurantMenu;
import asw.efood.restaurantservice.domain.RestaurantService; 

@Controller
@RequestMapping(path="/web")
public class RestaurantWebController {

	@Autowired 
	private RestaurantService restaurantService; 
	
	@Autowired
	private MenuService menuService;
	
	@Autowired
	private MenuItemService menuItemService;
	
	/* Trova il ristorante con restaurantId. */ 
	@GetMapping("/restaurants/{restaurantId}")
	public String getRestaurant(Model model, @PathVariable Long restaurantId) {
		Restaurant restaurant = restaurantService.getRestaurant(restaurantId);
		model.addAttribute("restaurant", restaurant);
		return "get-restaurant";
	}
	
	/* Trova il menu di un ristorante con restaurantId. */
	@GetMapping("/menu/{menuId}")
	public String getMenu(Model model, @PathVariable Long menuId) {
		RestaurantMenu menu = menuService.getMenu(menuId);
		model.addAttribute("menu",menu);
		return "get-menu"; 
	}

	/* Trova tutti i ristoranti. */ 
	@GetMapping("/restaurants")
	public String getRestaurants(Model model) {
		Collection<Restaurant> restaurants = restaurantService.getAllRestaurants();
		model.addAttribute("restaurants", restaurants);
		return "get-restaurants";
	}
	
	/* Crea un nuovo ristorante (form). */ 
	@GetMapping(value="/restaurants", params={"add"})
	public String getAddRestaurantForm(Model model) {
		model.addAttribute("form", new AddRestaurantForm());
		return "add-restaurant-form";
	}
	
	/* Crea un nuovo menu e restituisce il menu. */
	@GetMapping(value="/menu/{restaurantId}", params={"add"})
	public String addAndGetMenu(Model model,@PathVariable Long restaurantId) {
		Restaurant restaurant = restaurantService.getRestaurant(restaurantId);
		RestaurantMenu menu = menuService.createMenu(restaurant,new ArrayList<MenuItem>());
		restaurant.setMenu(menu);
		restaurant = restaurantService.update(restaurant);
		model.addAttribute("menu", menu);
		model.addAttribute("restaurant", restaurant);
		return "get-menu";
	}
	
	
	/* restituisce un menuItem(form). */
	@GetMapping(value="/menuItem/{menuId}", params={"add"})
	public String getAddMenuItemForm(Model model,@PathVariable Long menuId) {
		RestaurantMenu menu = menuService.getMenu(menuId);
		model.addAttribute("menu",menu);
		model.addAttribute("form",new AddMenuItemForm());
		return "add-menu-item-form";
	}
	
	/* restituisce un menuItem(form) per la modifica. */
	@GetMapping(value="/menuItem/{menuItemId}", params={"modify"})
	public String getAddMenuItemFormModify(Model model,@PathVariable Long menuItemId) {
		MenuItem menuItem = menuItemService.getMenuItem(menuItemId);
		AddMenuItemForm menuItemForm = new AddMenuItemForm();
		menuItemForm.setId(menuItem.getId());
		menuItemForm.setName(menuItem.getName());
		menuItemForm.setPrice(menuItem.getPrice());
		model.addAttribute("menu",menuItem.getMenu());
		model.addAttribute("form",menuItemForm);
		return "add-menu-item-form";
	}
	
	/* Crea un nuovo ristorante. */ 
	@PostMapping("/restaurants")
	public String addRestaurant(Model model, @ModelAttribute("form") AddRestaurantForm form) {
		Restaurant restaurant = restaurantService.createRestaurant(form.getName(), form.getLocation());
		model.addAttribute("restaurant", restaurant);
		return "get-restaurant";
	}	
	
	/* Crea un nuovo menuItem. */ 
	@PostMapping("/menu/{menuId}")
	public String addMenuItem(Model model, @ModelAttribute("form") AddMenuItemForm form,@PathVariable Long menuId) {
		RestaurantMenu menu = menuService.getMenu(menuId);
		MenuItem menuItem = null;
		
		if(form.getId() != null) {
			menuItem = menuItemService.getMenuItem(form.getId());
			menuItem.setName(form.getName());
			menuItem.setPrice(form.getPrice());
			menuItem = menuItemService.updateMenuItem(menuItem);
		} else {		
			menuItem = menuItemService.createMenuItem(form.getName(), form.getPrice(),menu);
		}
		
		menu = menuService.getMenu(menuId);
		model.addAttribute("menu", menu);

		return "get-menu";
	}	
}
