package asw.efood.restaurantservice.web;

import lombok.*; 

@Data 
public class AddRestaurantForm {

	private String name; 
	private String location;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public AddRestaurantForm() {
		super();
	}
}
