package asw.efood.restaurantservice.web;

import lombok.*; 

@Data 
public class AddMenuItemForm {

	private Long id;
	private String name;
	private double price;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public AddMenuItemForm() {
		super();
	}	
}
